import 'package:flutter/material.dart';
// import 'package:hola_mundo/src/pages/home_page.dart';

import 'pages/home_page.dart';
import 'pages/contador_page.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Center(
      //child: HomePage(),
      child: ContadorPage(),
    ));
  }
}

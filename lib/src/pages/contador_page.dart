import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  createState() {
    return _ContadorPageState();
  }
}

class _ContadorPageState extends State<ContadorPage> {
  int _conteo = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Stateful',
      theme: ThemeData(
        primaryColor: Color(0xFF151026),
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Stateful'),
        ),
        body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Número de clicks', style: TextStyle(fontSize: 25)),
                Text('$_conteo', style: TextStyle(fontSize: 25))
              ]),
        ),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            FloatingActionButton(
              child: Icon(Icons.add),
              tooltip: "Aumentar 1",
              onPressed: () => {
                setState(() {
                  _conteo++;
                })
              },
            ),
            FloatingActionButton(
                child: Icon(Icons.delete),
                tooltip: "Reducir en 1",
                onPressed: () => {
                      if (_conteo == 0)
                        {}
                      else
                        {
                          setState(() {
                            _conteo--;
                          })
                        }
                    }),
            FloatingActionButton(
                child: Icon(Icons.redo),
                tooltip: "Resetear contador",
                backgroundColor:  Color(0xFF111026),
                onPressed: () => {
                      setState(() {
                        _conteo = 0;
                      })
                    }),
          ],
        ),
      ),
    );
  }
}

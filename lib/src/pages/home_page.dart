
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget{
  // lab10:
  /*@override
  Widget build(BuildContext context){

    return Scaffold(appBar: AppBar(
      title: Text('Titulo'),
      centerTitle: true
    ),
    body: Center(
      child: Text('Hola mundo'),
    ),
    );
  }*/

  // lab11:
  // final estiloTexto = new TextStyle(fontSize:25)
  final conteo =10;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Número de clics', style: TextStyle(fontSize: 25)),
              Text('$conteo', style: TextStyle(fontSize: 25))
            ],
          ),
        ),

        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: ()=>{
            print('Hola mundo')
            //conteo = conteo + 1
          },
        )

      )
    );
  }
}